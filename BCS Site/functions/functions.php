<?php

include_once('creds.php');
date_default_timezone_set("Pacific/Auckland");

function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

function pageselect() {
    
    $conn = connection();
    if($conn-> connect_error){
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT Name from page";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()){
        echo "<option value='". $row['Name']."'>". $row['Name']. "</option>";
    }
    $conn->close();
}

function pageselectupdate() {
    
    $conn = connection();
    if($conn-> connect_error){
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT Name from page WHERE name NOT LIKE 'All'";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()){
        echo "<option value='". $row['Name']."'>". $row['Name']. "</option>";
    }
    $conn->close();
}

function typeselect() {
    
    $conn = connection();
    if($conn-> connect_error){
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT Name from type";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()){
        echo "<option value='". $row['Name']."'>". $row['Name']. "</option>";
    }
    $conn->close();
}

function pageparentselect() {
    
    $conn = connection();
    if($conn-> connect_error){
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT Name from page WHERE Parent IS NULL";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()){
        echo "<option value='". $row['Name']."'>". $row['Name']. "</option>";
    }
    $conn->close();
}


function loginAdmin(){
    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['Name']);
        $pass = $db->real_escape_string($_POST['Password']);

        $sql = "SELECT * FROM admin WHERE Name = '$user' && Password = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "Name" => $row['Name'],
                "Password" => $row['Password']
            );  
        }

        if(mysqli_num_rows($result)>0){
            if (($user == $arr[0]['Name']) && ($pass == $arr[0]['Password']))
            {
                $_SESSION['login'] = TRUE;
                redirect("admin/index.php");
            }
        
        }
        else
        {
            
            echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
        }
        $result->free();
        $db->close();

    }
}

function show_content(){
    $page = $_POST['page'];
    echo $page;
}
function get_content($pagename, $parent){
    $db = connection();
    $sql = "SELECT * FROM content WHERE pagename ='$pagename' AND parent ='$parent'";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['id'],
            "pagename" => $row['pagename'],
            "order" => $row['order'],
            "type" => $row['type'],
            "heading" => $row['heading'],
            "content" => $row['content'],
            "linksrc" => $row['linksrc'],
            "alt" => $row['alt'],
            "parent" => $row['parent']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}


function redirect($location)
    {
    $URL = $location;
    echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
    echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
    exit();
}

function logout()
    {
    if(isset($_POST['logout']))
    {
        

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        redirect("../index.php");
    }
}


    ?>