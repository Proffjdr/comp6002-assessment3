<?php
include_once('creds.php');
date_default_timezone_set("Pacific/Auckland");

$sql = json_decode($_POST["user"], false);

$content = array();

$conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $content[] = $row;
    }
} else {
    echo "no";
}
header('Content-type: application/json');
echo json_encode($content);
$conn->close();

?>