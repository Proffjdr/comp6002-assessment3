<!doctype html>
<html lang="en">
	
	<!-- HEAD -->
	<head>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PandoraLab</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css" type="text/css" >
    </head>
	
	<!-- BODY -->
    <body>
	
		<!-- BODY CONTAINER -->
		<div class="container">
		
			<!-- HEADER -->
			<header>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">  
						<img src="images/Banner.jpg" alt="Toi Ohomai Banner">
					</div>
				</div>
			</header>
		
			<!-- NAV -->
			<div class="row" id="navrow">   
                <div class="col-sm-10 col-sm-offset-1" id="nopaddingtop"> 
					<nav class="navbar">
					
						<!-- NAV HEADER -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="index.html">PandoraLab</a>
						</div>
						
						<!-- NAV LINKS -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right" id="navigation">
							</ul>
						</div>
					</nav>
				</div>
			</div>
		
			<!-- MAIN BODY-->
			<div class="row" id="outerbodybg">   
				<div class="col-sm-10 col-sm-offset-1" id="bodybg"> 
					<img src="images/Pandora.png" id="marginbottom" alt="Pandora Banner">
					<div id ="content"></div>
				</div>
			</div>
			
			<!-- FOOTER -->
			<footer>
				<div class="row">   
					<div class="col-sm-10 col-sm-offset-1">
						<div class="row">
							<div class="col-sm-6">  
								<img src="images/ToiOhomai.png" alt="Toi Ohomai Logo">
							</div>
							<div class="col-sm-6">  
								<img src="images/Waikato.gif" alt="Waikato Uni Logo">
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		
		<!-- SCRIPTS -->
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="scripts/script.js"></script>
    </body>
</html>