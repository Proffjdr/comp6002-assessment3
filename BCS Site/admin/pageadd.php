<?php   include_once('../functions/functions.php');
        session_start();
        //logout();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS ADMIN</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/admin.css" type="text/css" >
        <!--<link rel="stylesheet" href="../css/customstyles.css" type="text/css" >-->
    </head>
    <body class="backing" onload="logincheck()">
    <div class="container">
        <div class="row">
            <header class="page-header" id="nametitle">
                    <h1>BCS Admin Add Page</h1>
                </header>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="adminhome.php" class="btn btn-primary">Back</a>
            </div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <label for='nameBox'>Name:</label><input type='text' class='form-control' id='nameBox'>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <label for='orderBox'>Order:</label><input type='text' class='form-control' id='orderBox'>
            </div>
        </div>
        <div class ="row spacer">
            <div class="col-sm-6  col-sm-offset-3">
                <label for="pageparentselect">Submenu of:</label>
                <select name= "Page" class="form-control selectpicker" id="parentselect" Required>";
                    <option value="None">None</option>
                    <?PHP pageparentselect();?>
                </select>
            </div>        
        </div>
        <div class="row">
            <div class="col-sm-3 col-sm-offset-9"><button class='btn btn-primary' onclick='CreatePage()'>Create Page</button></div>
        </div>
    </div>
        <!-- Content ends here -->
    <script src="../scripts/admin.js"></script>
    
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
