<?php   include_once('../functions/functions.php');
        session_start();
        //logout();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS ADMIN</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/admin.css" type="text/css" >
        <!--<link rel="stylesheet" href="../css/customstyles.css" type="text/css" >-->
    </head>
    <body class="backing">
    <div class="container">
        <div class="row">
            <header class="page-header">
                <h1>BCS Admin Login</h1>
            </header>
        </div>
        <div class="row spacer">
            <div class="col-sm-8 col-sm-offset-2">
                <label for='user'>Username:</label><input type='text' class='form-control' id='user'>
            </div>
        </div>
        <div class="row spacer">
            <div class="col-sm-8 col-sm-offset-2">
                <label for='pass'>Password:</label><input type='password' class='form-control' id='pass'>
            </div>
        </div>
        <div class="row spacer">
            <div class="col-sm-8 col-sm-offset-2">
                <button class="btn btn-primary" onclick="AdminLogin();">Login</button>
            </div>
        </div>
    </div>
        <!-- Content ends here -->
    <script src="../scripts/admin.js"></script>
    
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
