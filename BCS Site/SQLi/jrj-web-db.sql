-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 08, 2017 at 09:42 AM
-- Server version: 5.6.34-log
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jrj-web-db`
--
CREATE DATABASE IF NOT EXISTS `jrj-web-db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `jrj-web-db`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `Name` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Name`, `Password`) VALUES
('admin', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `Name` varchar(20) NOT NULL,
  `Page` varchar(20) NOT NULL,
  `Order` int(11) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `Content` varchar(1000) DEFAULT NULL,
  `Link` varchar(200) DEFAULT NULL,
  `Alt` varchar(50) DEFAULT NULL,
  `Parent` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`Name`, `Page`, `Order`, `Type`, `Content`, `Link`, `Alt`, `Parent`) VALUES
('Img_Imagine', 'Links', 7, 'IMGSmall', '../images/MI.jpg', 'http://e5.onthehub.com/WebStore/ProductsByMajorVersionList.aspx?ws=20830094-5e9b-e011-969d-0030487d8897', 'Microsoft Imagine.', NULL),
('Img_Moodle', 'Links', 2, 'IMGSmall', '../images/Moodle.jpg', 'http://moodle2.boppoly.ac.nz/', 'Moodle.', NULL),
('Img_Office', 'Links', 4, 'IMGSmall', '../images/Office365.jpg', 'https://portal.office.com/OLS/MySoftware.aspx', 'Office 365.', NULL),
('Img_Outlook', 'Links', 3, 'IMGSmall', '../images/Outlook.jpg', 'http://www.outlook.com/stu.boppoly.ac.nz', 'Outlook.', NULL),
('Img_TO', 'Links', 6, 'IMGSmall', '../images/TO.jpg', 'https://getconnected.boppoly.ac.nz/go/myplace', 'Toi Ohomai.', NULL),
('Img_Visual_Studio', 'Links', 5, 'IMGSmall', '../images/VS.jpg', 'https://go.microsoft.com/fwlink/?LinkId=691978&clcid=0x409', 'Visual Studio.', NULL),
('Jmb_Google', 'Google Apps', 1, 'Jumbotron', 'Google Apps Setup', NULL, NULL, NULL),
('Jmb_Imagine', 'Microsoft Imagine', 1, 'Jumbotron', 'Microsoft Imagine', NULL, NULL, NULL),
('Jmb_Links', 'Links', 1, 'Jumbotron', 'Links', NULL, NULL, NULL),
('Jmb_Pandora', 'Pandora', 1, 'Jumbotron', 'What is different in Pandora?', NULL, NULL, NULL),
('Jmb_Slack', 'Slack', 1, 'Jumbotron', 'Setup Slack Account', NULL, NULL, NULL),
('Jmb_Welcome', 'Home', 1, 'Jumbotron', 'Welcome to BCS.NET.NZ', NULL, NULL, NULL),
('Jmb_Welcome_P1', 'Home', 1, 'P', 'Toi Ohomai Institute of Technology Bachelor of Computing and Mathematical Sciences', NULL, NULL, 'Jmb_Welcome'),
('Jmb_Welcome_P2', 'Home', 2, 'P', 'This Intranet is for students who are completing the Bachelor of Computer Science in Applied Science.', NULL, NULL, 'Jmb_Welcome'),
('Jmb_Welcome_P3', 'Home', 3, 'P', 'On this little mini-site you can find information specific to the Pandora labs that are used as part of your course.', NULL, NULL, 'Jmb_Welcome'),
('Pnl_About_Imagine', 'Microsoft Imagine', 2, 'PanelBig', 'What is Microsoft Imagine?', NULL, NULL, NULL),
('Pnl_About_Imagine_P1', 'Microsoft Imagine', 1, 'P', 'At the beginning of each semester we set the students up with a Microsoft Imagine Account.', NULL, NULL, 'Pnl_About_Imagine'),
('Pnl_About_Imagine_P2', 'Microsoft Imagine', 2, 'P', 'This allows you to download some free products from Microsoft that you would otherwise have to pay for.', NULL, NULL, 'Pnl_About_Imagine'),
('Pnl_About_Imagine_P3', 'Microsoft Imagine', 3, 'P', 'Once you have graduated or are no longer with us, you can keep the software and licenses, but you will not be able to access the website anymore.', NULL, NULL, 'Pnl_About_Imagine'),
('Pnl_Apps', 'Slack', 3, 'PanelBig', 'Slack apps for your device', NULL, NULL, NULL),
('Pnl_Apps_Account', 'Google Apps', 2, 'PanelBig', 'Setting up your Google Apps Account', NULL, NULL, NULL),
('Pnl_Apps_Account_P1', 'Google Apps', 1, 'P', 'At the beginning of each semester, all the passwords are reset to 12345678.', NULL, NULL, 'Pnl_Apps_Account'),
('Pnl_Apps_Account_P2', 'Google Apps', 2, 'P', 'When you sign in with Chrome you will need to know your student Id.', NULL, NULL, 'Pnl_Apps_Account'),
('Pnl_Apps_Account_P3', 'Google Apps', 3, 'P', 'Your Google Apps ID = student-id@bcs.net.nz', NULL, NULL, 'Pnl_Apps_Account'),
('Pnl_Apps_Account_P4', 'Google Apps', 4, 'P', 'Your password is 12345678 (You will be asked to change this after you accept the terms and conditions)', NULL, NULL, 'Pnl_Apps_Account'),
('Pnl_Apps_IMG1', 'Slack', 4, 'IMGMedium', 'images/GooglePlay.png', 'https://play.google.com/store/apps/details?id=com.Slack&hl=en', 'Google Play', 'Pnl_Apps'),
('Pnl_Apps_IMG2', 'Slack', 5, 'IMGMedium', 'images/AppStore.png', 'https://www.google.com/url?q=https%3A%2F%2Fitunes.apple.com%2Fnz%2Fapp%2Fslack-business-communication-for-teams%2Fid618783545%3Fmt%3D8&sa=D&sntz=1&usg=AFQjCNGUk0rvmUs6PbJVLeTWUUCBnFx2Xg', 'App Store.', 'Pnl_Apps'),
('Pnl_Apps_P1', 'Slack', 1, 'P', 'Slack has a (mobile) app for every platform basically.', NULL, NULL, 'Pnl_Apps'),
('Pnl_Apps_P2', 'Slack', 2, 'P', 'Simply go to https://slack.com/downloads and it will detect what kind of system you are using.', NULL, NULL, 'Pnl_Apps'),
('Pnl_Apps_P3', 'Slack', 3, 'P', 'If you are on an iOS or Android device, you look up slack in the App Store or Google Play Store.', NULL, NULL, 'Pnl_Apps'),
('Pnl_Home_Access', 'Pandora', 4, 'PanelSmall', 'Can I access anything on the Pandora Network from Home?', NULL, NULL, NULL),
('Pnl_Home_Access_P1', 'Pandora', 1, 'P', 'Simple answer - No you cannot.', NULL, NULL, 'Pnl_Home_Access'),
('Pnl_Home_Access_P2', 'Pandora', 2, 'P', 'However your course material is on Moodle and you can put files on Google drive and you are able to access what you need from there.', NULL, NULL, 'Pnl_Home_Access'),
('Pnl_Home_Access_P3', 'Pandora', 3, 'P', 'In terms of software, everything we use in the labs is available for free either through MS Imagine or a student service or the program is actually free to download.', NULL, NULL, 'Pnl_Home_Access'),
('Pnl_Pandora_Info', 'Home', 2, 'PanelSmall', 'Information for Pandora Network', NULL, NULL, NULL),
('Pnl_Pandora_Info_Ul', 'Home', 1, 'Ul', NULL, NULL, NULL, 'Pnl_Pandora_Info'),
('Pnl_Pandora_Info_Ul1', 'Home', 1, 'Li', 'Setup your Google Apps For Education Account', 'Google Apps', NULL, 'Pnl_Pandora_Info_Ul'),
('Pnl_Pandora_Info_Ul2', 'Home', 2, 'Li', 'Setup your Slack Account', 'Slack', NULL, 'Pnl_Pandora_Info_Ul'),
('Pnl_Pandora_Info_Ul3', 'Home', 3, 'Li', 'Setup your Microsoft Imagine Account', 'Microsoft Imagine', NULL, 'Pnl_Pandora_Info_Ul'),
('Pnl_Pandora_Info_Ul4', 'Home', 4, 'Li', 'Corporate Network and Pandora - What is the difference?', 'Pandora', NULL, 'Pnl_Pandora_Info_Ul'),
('Pnl_Profile', 'Google Apps', 2, 'PanelBig', 'Setting up your Google Profile', NULL, NULL, NULL),
('Pnl_Profile_IMG', 'Google Apps', 4, 'IMGBig', '../images/Google.gif', 'https://cl.ly/jIld', 'Signing into chrome.', 'Pnl_Profile'),
('Pnl_Profile_P1', 'Google Apps', 1, 'P', 'With chrome you can sign into your google account from the browser app and on the webpage. If you do it from the browser app, then it will create a link to your google profile and sign into your personal account and the school account at the same time.', NULL, NULL, 'Pnl_Profile'),
('Pnl_Profile_P2', 'Google Apps', 2, 'P', 'Have a look at this gif to see how to sign into Chrome.', NULL, NULL, 'Pnl_Profile'),
('Pnl_Profile_P3', 'Google Apps', 3, 'P', 'Click on the image to make it bigger.', NULL, NULL, 'Pnl_Profile'),
('Pnl_Setup_Imagine', 'Microsoft Imagine', 2, 'PanelBig', 'Setting up your Microsoft Imagine Account', NULL, NULL, NULL),
('Pnl_Setup_Imagine_IM', 'Microsoft Imagine', 4, 'IMGBig', '../images/Setup-MSImagine.gif', 'https://cl.ly/jIF0', 'Setting Up Microsoft Imagine.', 'Pnl_Setup_Imagine'),
('Pnl_Setup_Imagine_P1', 'Microsoft Imagine', 1, 'P', 'In your bcs.net.nz email (it is a gmail account) you should have an email from Toi Ohomai and the subject should say "Your school created a Microsoft Imagine WebStore account for you."', NULL, NULL, 'Pnl_Setup_Imagine'),
('Pnl_Setup_Imagine_P2', 'Microsoft Imagine', 2, 'P', 'In the image below you can see how to setup your account.', NULL, NULL, 'Pnl_Setup_Imagine'),
('Pnl_Setup_Imagine_P3', 'Microsoft Imagine', 3, 'P', 'Click on it to make it bigger.', NULL, NULL, 'Pnl_Setup_Imagine'),
('Pnl_Setup_Slack', 'Slack', 2, 'PanelBig', 'Setting up your Slack Account', NULL, NULL, NULL),
('Pnl_Setup_Slack_IMG', 'Slack', 4, 'IMGBig', '../images/Slack.gif', 'https://cl.ly/jIaS', 'Signing Into Slack.', 'Pnl_Setup_Slack'),
('Pnl_Setup_Slack_P1', 'Slack', 1, 'P', 'Within the IT Team and some of your courses we use slack to communicate.', NULL, NULL, 'Pnl_Setup_Slack'),
('Pnl_Setup_Slack_P2', 'Slack', 2, 'P', 'To create your account simply go to https://to-bcs.slack.com and sign in with your Google Apps Account.', NULL, NULL, 'Pnl_Setup_Slack'),
('Pnl_Setup_Slack_P3', 'Slack', 3, 'P', 'Click on the image to make it bigger', NULL, NULL, 'Pnl_Setup_Slack'),
('Pnl_Software', 'Microsoft Imagine', 4, 'PanelBig', 'Download Software from Microsoft Imagine', NULL, NULL, NULL),
('Pnl_Software_IMG', 'Microsoft Imagine', 3, 'IMGBig', '../images/Download-From-MSImagine.gif', 'https://cl.ly/jIQ0', 'Download From Microsoft Imagine.', 'Pnl_Software'),
('Pnl_Software_P1', 'Microsoft Imagine', 1, 'P', 'The image below shows how to download software from Microsoft Imagine.', NULL, NULL, 'Pnl_Software'),
('Pnl_Software_P2', 'Microsoft Imagine', 2, 'P', 'Click on it to make it bigger.', NULL, NULL, 'Pnl_Software'),
('Pnl_Studnt_Offers', 'Home', 3, 'PanelSmall', 'General Student Offers', NULL, NULL, NULL),
('Pnl_Studnt_Offers_P1', 'Home', 1, 'P', 'As a student of Toi Ohomai Institute of Technology, you also gain the benefits the students gain from other courses.', NULL, NULL, 'Pnl_Studnt_Offers'),
('Pnl_Studnt_Offers_P2', 'Home', 2, 'P', 'Most of them are listed on the getconnected website, but here are the most common parts.', NULL, NULL, 'Pnl_Studnt_Offers'),
('Pnl_Studnt_Offers_U', 'Home', 3, 'Ul', NULL, NULL, NULL, 'Pnl_Studnt_Offers'),
('Pnl_Studnt_Offers_U1', 'Home', 1, 'Li', 'Access Moodle', 'https://moodle2.boppoly.ac.nz/', NULL, 'Pnl_Studnt_Offers_U'),
('Pnl_Studnt_Offers_U2', 'Home', 2, 'Li', 'Download Office 2016 as part of Office 365', 'https://www.boppoly.ac.nz/go/office-365-students', NULL, 'Pnl_Studnt_Offers_U'),
('Pnl_Studnt_Wifi', 'Home', 4, 'PanelSmall', 'Using the Student Wi-Fi', NULL, NULL, NULL),
('Pnl_Studnt_Wifi_Ol', 'Home', 2, 'Ol', NULL, NULL, NULL, 'Pnl_Studnt_Wifi'),
('Pnl_Studnt_Wifi_Ol_1', 'Home', 1, 'Li', 'Select Toi Ohomai Wi-Fi in your Wifi Settings', NULL, NULL, 'Pnl_Studnt_Wifi_Ol'),
('Pnl_Studnt_Wifi_Ol_2', 'Home', 2, 'Li', 'Username = moodle-id@stu.boppoly.ac.nz', NULL, NULL, 'Pnl_Studnt_Wifi_Ol'),
('Pnl_Studnt_Wifi_Ol_3', 'Home', 3, 'Li', 'Password = your student id (unless you have changed it)', NULL, NULL, 'Pnl_Studnt_Wifi_Ol'),
('Pnl_Studnt_Wifi_P1', 'Home', 1, 'P', 'To connect your device to the Student Wifi do this:', NULL, NULL, 'Pnl_Studnt_Wifi'),
('Pnl_Wifi_Network', 'Pandora', 3, 'PanelSmall', 'Toi Ohomai Wi-Fi Network', NULL, NULL, NULL),
('Pnl_Wifi_Network_P1', 'Pandora', 1, 'P', 'The student Wi-Fi is also completely separate from the Pandora Network. If you are using your own device or connect your mobile device, the easiest way to access your files is by using Google Apps', NULL, NULL, 'Pnl_Wifi_Network'),
('Pnl_Wifi_Network_P2', 'Pandora', 2, 'P', 'The Wifi network basically allows you to browse the web. There are some exceptions, but you will hear about that in the Web Development papers you will do in your degree.', NULL, NULL, 'Pnl_Wifi_Network'),
('Pnl_Your_files', 'Pandora', 2, 'PanelSmall', 'Your files', NULL, NULL, NULL),
('Pnl_Your_Files_P1', 'Pandora', 1, 'P', 'The corporate network (the library and classrooms other than the 3rd floor) is completely separate from the Pandora network (DT219, DT303, DT308 and DT312)', NULL, NULL, 'Pnl_Your_files'),
('Pnl_Your_Files_P2', 'Pandora', 2, 'P', 'Both networks have an network drive for you to store you files in and both networks refer to that drive as H-Drive, however they are different and do not relate to each other in anyway.', NULL, NULL, 'Pnl_Your_files'),
('Pnl_Your_Files_P3', 'Pandora', 3, 'P', 'By using your Google Apps account you are able to access your files from any machine using a web browser and you will not be limited by this.', NULL, NULL, 'Pnl_Your_files'),
('Questions_Header', 'All', 100, 'H3', 'Any Questions?', NULL, NULL, NULL),
('Questions_P1', 'All', 101, 'P', 'If you have any questions, ask any of the IT staff and we can help you out.', NULL, NULL, NULL),
('Questions_P2', 'All', 102, 'P', 'You can also post your questions in Slack, in the #general channel, and other students can help out as well!', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `Name` varchar(20) NOT NULL,
  `Order` int(11) NOT NULL,
  `Parent` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`Name`, `Order`, `Parent`) VALUES
('All', 0, NULL),
('Google Apps', 1, 'Setup'),
('Home', 1, NULL),
('Links', 3, NULL),
('Microsoft Imagine', 3, 'Setup'),
('Pandora', 2, NULL),
('Setup', 4, NULL),
('Slack', 2, 'Setup');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`Name`) VALUES
('H3'),
('IMGBig'),
('IMGMedium'),
('IMGSmall'),
('Jumbotron'),
('Li'),
('Ol'),
('P'),
('PanelBig'),
('PanelSmall'),
('Ul');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Name`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`Name`),
  ADD KEY `Page` (`Page`),
  ADD KEY `Parent` (`Parent`),
  ADD KEY `Type` (`Type`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`Name`),
  ADD KEY `Parent` (`Parent`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`Name`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `FK_Content` FOREIGN KEY (`Parent`) REFERENCES `content` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Page` FOREIGN KEY (`Page`) REFERENCES `page` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Type` FOREIGN KEY (`Type`) REFERENCES `type` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_Parent` FOREIGN KEY (`Parent`) REFERENCES `page` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
